#!/bin/sh

# ------------------------------------------------------------------------------
# shell-script for initial setup of the slim-theme
# used to configure the screen-resolution for the background
# ------------------------------------------------------------------------------

if [ "$(id -u)" != 0 ]; then
	echo "Please run this script with root-privileges." >&2; exit 1;
fi

cd "`dirname "$0"`"

resolutions=(
 1024x768
 1280x720
 1280x800
 1360x768
 1366x768
 1440x900
 1600x900
 1680x1050
 1920x1080
 1920x1200
)
selected_resolution=$(dialog --no-tags --no-cancel --clear --backtitle "Setup for the Hyperbola SLiM theme" \
                             --radiolist "Choose your concurrent screen-resolution used for the background-image:" 15 40 30 \
                             0 "1024x768" on \
                             1 "1280x720" off \
                             2 "1280x800" off \
                             3 "1360x768" off \
                             4 "1366x768" off \
                             5 "1440x900" off \
                             6 "1600x900" off \
                             7 "1680x1050" off \
                             8 "1920x1080" off \
                             9 "1920x1200" off 2>&1 > /dev/tty)

src_filename=background_${resolutions[$selected_resolution]}.png
cp $src_filename background.png
clear